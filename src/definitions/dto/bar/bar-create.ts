import {IsArray, IsMongoId, IsString, MinLength, ValidateNested} from "class-validator";
import {Expose} from "class-transformer";

export class BarCreateDTO {

    @MinLength(1)
    @IsString()
    @Expose()
    location: string;

    @IsString()
    @Expose()
    @MinLength(1)
    address: string;

    @IsArray()
    @Expose()
    cocktails: string[];
}