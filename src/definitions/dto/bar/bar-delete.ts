import {IsMongoId} from "class-validator";
import {Expose} from "class-transformer";

export class BarDeleteDTO {
    @IsMongoId()
    @Expose()
    id: string;
}