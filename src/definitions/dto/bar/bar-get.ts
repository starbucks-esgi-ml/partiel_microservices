import {IsMongoId, IsOptional, IsString, MinLength} from "class-validator";
import {Expose} from "class-transformer";

export class BarGetDTO {
    @IsMongoId()
    @Expose()
    @IsOptional()
    id?:string;

    @IsOptional()
    @MinLength(1)
    @IsString()
    @Expose()
    location?: string;

    @IsOptional()
    @IsString()
    @Expose()
    @MinLength(1)
    address?: string;

    @IsOptional()
    @IsString({each: true})
    @IsMongoId()
    @Expose()
    cocktails?: string[];
}