import {IsMongoId, IsOptional, IsString, MinLength} from "class-validator";
import {Expose} from "class-transformer";

export class BarSearchCocktailDTO {
    @IsMongoId()
    @Expose()
    id:string;

    @IsString()
    @MinLength(1)
    @IsOptional()
    research?: string;
}