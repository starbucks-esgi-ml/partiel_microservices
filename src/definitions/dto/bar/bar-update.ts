import {IsArray, IsMongoId, IsOptional, IsString, MinLength, ValidateNested} from "class-validator";
import {Expose, Type} from "class-transformer";

export class BarUpdateDTO {
    @IsMongoId()
    @Expose()
    id:string;

    @IsOptional()
    @MinLength(1)
    @IsString()
    @Expose()
    location?: string;

    @IsOptional()
    @IsString()
    @Expose()
    @MinLength(1)
    address?: string;

    @IsOptional()
    @IsArray()
    @Expose()
    cocktails?: string[];
}