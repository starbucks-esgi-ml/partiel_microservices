import {IsNumber, IsString, Min, MinLength} from "class-validator";
import {Expose} from "class-transformer";

export class CocktailCreateDTO {

    @MinLength(4)
    @IsString()
    @Expose()
    name: string;

    @IsNumber()
    @Expose()
    @Min(0)
    price: number;

    @IsString()
    @Expose()
    @MinLength(1)
    alcool: string;

    @IsString()
    @Expose()
    @MinLength(1)
    ingredients: string;

    @IsString()
    @Expose()
    @MinLength(1)
    description: string;
}