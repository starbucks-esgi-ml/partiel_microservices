import {IsMongoId} from "class-validator";
import {Expose} from "class-transformer";

export class CocktailDeleteDTO {
    @IsMongoId()
    @Expose()
    id: string;
}