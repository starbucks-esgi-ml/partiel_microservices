import {IsNumber, IsOptional, IsString, MaxLength, Min, MinLength} from "class-validator";
import {Expose} from "class-transformer";

export class CocktailUpdateDTO {
    @IsString()
    @Expose()
    @MinLength(24)
    @MaxLength(24)
    id: string;

    @IsOptional()
    @MinLength(4)
    @IsString()
    @Expose()
    name?: string;

    @IsOptional()
    @IsNumber()
    @Expose()
    @Min(0)
    price?: number;

    @IsOptional()
    @IsString()
    @Expose()
    @MinLength(1)
    alcool?: string;

    @IsString()
    @Expose()
    @MinLength(1)
    @IsOptional()
    ingredients?: string;

    @IsString()
    @Expose()
    @MinLength(1)
    @IsOptional()
    description?: string;
}