import {ICocktail} from "./cocktail.interface";

export interface IBar{
    _id: any;
    location: string;
    address: string;
    cocktails: ICocktail[];
}