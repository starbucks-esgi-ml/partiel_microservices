export interface ICocktail{
    _id: any;
    name: string;
    price: number;
    alcool: string;
    ingredients: string;
}