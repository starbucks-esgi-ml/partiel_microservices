import {BaseController} from "./base.controller";
import {Router, Request, Response} from "express";
import {MongooseUtils} from "../../mongoose";
import {BarCreateDTO, BarDeleteDTO, BarGetDTO, BarUpdateDTO} from "../../definitions";
import {BarSearchCocktailDTO} from "../../definitions/dto/bar/bar-search-cocktail";

export class BarController extends BaseController{

    async create(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarCreateDTO, req.body,res);
        if(!dto) return;

        try{
            const bar = await this.serviceRegistry.barService.create(dto);
            res.status(201).json(bar);
        }catch(err){
            if(MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end();
            }

            console.log(err);
            res.status(500).end();
        }
    }

    async update(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarUpdateDTO, req.body,res);
        if(!dto) return;

        try{
            const apiRes = await this.serviceRegistry.barService.update(dto);
            if(apiRes.acknowledged && apiRes.modifiedCount === 1){
                res.status(201).json(dto);
                return;
            }

            if(apiRes.acknowledged){
                res.status(204).json(dto);
                return;
            }

            res.status(404).json({
                message: "Bar not found"
            })
        }catch(err){
            res.status(500).end();
        }
    }


    async delete(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarDeleteDTO, req.body,res);
        if(!dto) return;

        try{
            const apiRes = await this.serviceRegistry.barService.delete(dto);

            if(apiRes.acknowledged && apiRes.deletedCount === 1){
                res.status(204).json({
                    message: "Ressource successfully deleted"
                });
                return;
            }

            res.status(404).json({
                message: "Ressource not found"
            })
        }catch(err){
            console.log(err);
            res.status(500).end();
        }
    }

    async get(req: Request, res: Response){
        console.log(req.query);
        const dto = await BarController.createAndValidateDTO(BarGetDTO, req.query,res);
        if(!dto) return;

        try{
            const bars = await this.serviceRegistry.barService.get(req.query);
            res.status(200).json(bars)
        }catch(err){
            console.log(err);
            res.status(500).end();
        }
    }

    async searchCocktail(req: Request, res: Response){
        const dto = await BarController.createAndValidateDTO(BarSearchCocktailDTO, req.query,res);
        if(!dto) return;

        try{
            let bars = await this.serviceRegistry.barService.get(dto);
            bars = bars.filter((bar) => {
                for(let i = 0; i < bar.cocktails.length; i++){
                    if(bar.cocktails[i].name === req.query.research){
                        return true;
                    }
                }
                return false;
            })
            res.status(200).json(bars)
        }catch(err){
            console.log(err);
            res.status(500).end();
        }
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('',this.create.bind(this));
        router.put('',this.update.bind(this));
        router.delete('',this.delete.bind(this));
        router.get('',this.get.bind(this));
        router.get('/search-cocktail', this.searchCocktail.bind(this));
        return router;
    }
}