import {BaseController} from "./base.controller";
import {Router, Request, Response} from "express";
import {MongooseUtils} from "../../mongoose";
import {CocktailCreateDTO, CocktailDeleteDTO, CocktailGetDTO, CocktailUpdateDTO} from "../../definitions";

export class CocktailController extends BaseController{

    async create(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailCreateDTO, req.body,res);
        if(!dto) return;

        try{
            const cocktail = await this.serviceRegistry.cocktailService.create(dto);
            res.status(201).json(cocktail);
        }catch(err){
            if(MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end();
            }
            res.status(500).end();
        }
    }

    async update(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailUpdateDTO, req.body,res);
        if(!dto) return;

        try{
            const apiRes = await this.serviceRegistry.cocktailService.update(dto);
            if(apiRes.acknowledged && apiRes.modifiedCount === 1){
                res.status(201).json(dto);
                return;
            }

            if(apiRes.acknowledged){
                res.status(204).json(dto);
                return;
            }

            res.status(404).json({
                message: "Cocktail not found"
            })
        }catch(err){
            res.status(500).end();
        }
    }


    async delete(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailDeleteDTO, req.body,res);
        if(!dto) return;

        try{
            const apiRes = await this.serviceRegistry.cocktailService.delete(dto);

            if(apiRes.acknowledged && apiRes.deletedCount === 1){
                res.status(204).json({
                    message: "Ressource successfully deleted"
                });
                return;
            }

            res.status(404).json({
                message: "Ressource not found"
            })
        }catch(err){
            console.log(err);
            res.status(500).end();
        }
    }

    async get(req: Request, res: Response){
        const dto = await CocktailController.createAndValidateDTO(CocktailGetDTO, req.query,res);
        if(!dto) return;

        try{
            const cocktails = await this.serviceRegistry.cocktailService.get(req.query);
            res.status(200).json(cocktails)
        }catch(err){
            console.log(err);
            res.status(500).end();
        }
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('',this.create.bind(this));
        router.put('',this.update.bind(this));
        router.delete('',this.delete.bind(this));
        router.get('',this.get.bind(this));
        return router;
    }
}