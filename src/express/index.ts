import * as express from 'express';
import {BarController, CocktailController} from "./controllers";
import {ServiceRegistry} from "../mongoose";

export function startServer(serviceRegistry: ServiceRegistry): void{
    const app = express();
    app.use(express.json());

    const cocktailController = new CocktailController(serviceRegistry);
    const barController = new BarController(serviceRegistry);

    app.use('/cocktail', cocktailController.buildRoutes());
    app.use('/bar', barController.buildRoutes());
    app.listen(process.env.PORT, function() {
        console.log(`Server listening on port ${process.env.PORT}...`);
    })
}
