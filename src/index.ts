import 'reflect-metadata';
import {config} from "dotenv";
import {MongooseUtils, ServiceRegistry} from "./mongoose";
import {startServer} from "./express";
config();

async function main(){
    const connection = await MongooseUtils.connect();
    const serviceRegistry = new ServiceRegistry(connection);
    startServer(serviceRegistry);
}

main().catch(console.error);