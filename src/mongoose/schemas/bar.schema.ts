import {Schema, SchemaTypes} from "mongoose";
import {IBar} from "../../definitions";

export const BarSchema = new Schema({
    location: {
        type: SchemaTypes.String,
        required: true,
    },
    address: {
        type: SchemaTypes.String,
        required: true,
    },
    cocktails: [{ type : SchemaTypes.ObjectId, ref: 'Cocktail' }],
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'bar'
});

export type IBarDocument = IBar & Document;