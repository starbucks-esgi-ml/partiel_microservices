import {Schema, SchemaTypes} from "mongoose";
import {ICocktail} from "../../definitions";

export type ICocktailDocument = ICocktail & Document;

export const CocktailSchema = new Schema({
    name: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    },
    price: {
        type: SchemaTypes.Number,
        required: true,
        min: 0
    },
    alcool: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    },
    ingredients: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    },
    description: {
        type: SchemaTypes.String,
        required: true,
        minLength: 1
    }
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'cocktails'
});
