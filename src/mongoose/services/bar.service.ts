import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {IBarDocument, BarSchema} from "../schemas";
import {
    BarCreateDTO,
    BarDeleteDTO,
    BarGetDTO,
    BarUpdateDTO,
} from "../../definitions";
import {BarSearchCocktailDTO} from "../../definitions/dto/bar/bar-search-cocktail";

export class BarService extends BaseService{
    protected model: Model<IBarDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<IBarDocument>('Bar',BarSchema);
    }

    public create(dto: BarCreateDTO):Promise<IBarDocument>{
        return this.model.create(dto)
    }

    public delete(dto: BarDeleteDTO): Promise<any>{
        return this.model.deleteOne(dto).exec();
    }

    public update(dto: BarUpdateDTO): Promise<any>{
        return this.model.updateOne({
            id : dto.id
        },dto).exec();
    }

    public get(dto: BarGetDTO): Promise<any>{
        return this.model.find(dto).populate("cocktails").exec();
    }
}