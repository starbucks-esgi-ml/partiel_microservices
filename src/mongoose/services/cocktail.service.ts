import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {CocktailSchema, ICocktailDocument} from "../schemas";
import {CocktailCreateDTO, CocktailDeleteDTO, CocktailGetDTO, CocktailUpdateDTO} from "../../definitions";

export class CocktailService extends BaseService{
    protected model: Model<ICocktailDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<ICocktailDocument>('Cocktail',CocktailSchema);
    }

    public create(dto: CocktailCreateDTO):Promise<ICocktailDocument>{
        return this.model.create(dto)
    }

    public delete(dto: CocktailDeleteDTO): Promise<any>{
        return this.model.deleteOne(dto).exec();
    }

    public update(dto: CocktailUpdateDTO): Promise<any>{
        return this.model.updateOne({
            id : dto.id
        },dto).exec();
    }

    public get(dto: CocktailGetDTO): Promise<any>{
        return this.model.find(dto).exec();
    }
}