import {Mongoose} from "mongoose";
import {CocktailService} from "./cocktail.service";
import {BarService} from "./bar.service";

export class ServiceRegistry{
    public readonly  cocktailService: CocktailService;
    public readonly barService: BarService;

    constructor(connection: Mongoose){
        this.cocktailService = new CocktailService(connection);
        this.barService = new BarService(connection);
    }
}